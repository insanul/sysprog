-----------------------------
Log Week 13 System Programming
-----------------------------

# Hubungan antara module dan driver
Module merupakan program yang dapat di-load dan di-unload pada kernel tanpa harus melakukan proses reboot. Karena sifatnya tersebut, kita dapat menghemat memori atau sumber daya lainnya dengan meng-unload module saat sedang tidak diperlukan. Sementara itu, driver merupakan program yang berfungsi untuk mengendalikan hardware. Beberapa driver perlu di-include ke dalam kernel file, (terutama jika driver tersebut terkait langsung dengan proses booting atau fungsinya diperlukan terus- menerus selama sistem berjalan), sementara beberapa lainnya relatif lebih baik dijadikan module. Meskipun begitu, tidak semua module merupakan driver.
# Cara melakukan insert dan remove module
- Untuk melakukan insert module, dapat menggunakan command insmod <module-name> atau
modprobe <module-name>
- Untuk melakukan remove module, dapat menggunakan command rmmod <module-name> atau
modprobe -r <module-name>
- Untuk melihat semua module yang sedang ter-load, dapat menggunakan command lsmod, atau
dapat dikombinasikan dengan grep untuk mengecek module tertentu
# Jenis-jenis device
- Network device, direpresentasikan sebagai network interface, dapat dilihat dengan menggunakan
command ifconfig
- Block device, digunakan untuk memberikan aplikasi yang berjalan pada user-space akses ke raw
storage device, dapat dilihat pada folder /dev
- Character device, digunakan untuk memberikan aplikasi yang berjalan pada user-space akses ke
device-device lain, termasuk di antaranya input, sound, graphics, dan serial. Sama seperti block device, dapat dilihat pada folder /dev
# Perbedaan major number dan minor number
Major number menunjukkan driver yang digunakan untuk mengendalikan device, sehingga semua device yang dikendalikan oleh driver yang sama akan memiliki major number yang sama. Sementara itu, minor number menunjukkan instance dari device terkait. Informasi mengenai minor number digunakan oleh driver untuk membedakan setiap device yang ia kendalikan.
